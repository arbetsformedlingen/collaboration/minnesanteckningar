# 2022-01-21 Moln i Offentlig sektor - Opens Source Sweden

Anteckningar: Magnus Runesson

Ca 500 deltagare anordnat av Open Source Sweden

## Peter Nordström Skatteverket, Strateg IT-avdelningen - eSam-rapporten

Del 2 av rapporten vid midsommar.
Del 1 bevisa att det finns alternativ, del 2 är att visa att det verkligen funkar för verkliga fall

Idag pratar man om hur man använder Teams etc genom att säga hur man inte använder det (sekretess etc). Open Source lösningarna kanske inte är lika polerade, men du ska kunna göra allt. Då blir läget annorlunda.

Lev del 2 Teknik & test:

* Elements, rocketchat, mattermost (prio 1)
* Jitsi (Koppla på möte på chatten)
* Nextcloud (Koppla på dochantering på chatten)

Mycket fokus på informationssäkerhet.

Leva som man lär i gruppen på en svensk molnlösning med rocket, jitsi och nextcloud.
Gör gemensam bedömning som används som underlag för beslut.

Del 2 tänks redovisas som en mässa.

Mål en kravlista som duger för de med stora krav. Då borde de flesta ha samma eller lägre kravlista.

Med open source så kan man fokusera på upphandla leverans och inte produkt.
Leverantörerna skall integrera och bygga ut.

Digitalisering är mer än Team och Zoom, fixar vi inte en samarbetsplattform så kommer vi inte fixa det andra.

## Tomas Lagren, Tekniker tidigare arkitekt, FK - Hur använder FK containrar

https://github.com/forsakringskassan/

Sidonot: FK kör POC på containerplattform (Jitsi & Matrix/Element)

Levererar en Container Application Platform till samverkansmyndigheter på ett molnliknande sätt.
NIST definition av moln(delar):

* Automation (on-demand)
* Går komma åt från alla typer av devicer över internet
* Man skall inte behöva fundera på var det körs. Kapaciteten skall upplevas oändlig.

Utmaningar för myndighetsmoln:

* Juridiken kräver samverkansavtal, beställningsrutiner etc.
* Lösningarna får inte levereras över internet. Det FK bygger får inte ha beroende till internet.
* Resurs kan inte vara oändlig då det finns programvarulicenser. Vissa saker får inte dela hårdvara.
* Ekonomimodellen stödjer inte att ha hårdvara som outnyttjad resurs.

FK levererar:

* Openshift as a service (övrvakar, uppdaterar, support) - kapacitetsleverans
* Samverkansmyndighet väljer storlek, SLA etc. Samverkande myndighet skall inte behöva hålla på med infrastruktur.
* Fungerar utan internet, möjligt att exponera tjänster på internet.
* Multisite, HA, säkerhetsklassad personal

Mycket kräver access till internet, kräver anpassningar. Samma saker glömmer många att containrar inte får köra som root/administratör.

Teamet: 5 pers ansvarar för drift och utv av OpenShift. Uppgradering och patchar sker kontinuerligt.
~60 kluster och ~500 noder jämt delat mellan Openshift 3 och 4. V3 på väg att avvecklas.
Automatiserar allt för att få det lika (viktigare ään att spara tid) (Ansible & AWX som verktyg)

## Daniel Melin, Strateg, Skatteverket - Skatteverkets molnstrategi

Skatteverket är aktiva i Gaia-X.

Strategin finns inte. Den behövs inte. Molnet är bara en leveransform.

Det finns hotbilder samt myndigheter måste agera under lagarna. Sverige måste alltid fungera. Register som definierar vad Sverige är måste finnas.

Utländska moln-leverantörer kan bli tvingade att lämna ut information. Då har myndigheten outsourcat beslut av utlämnande av uppgift till företaget, det går inte.

Det är en realitet att USA har ett intresse att inhämta information om Sverige. (På samma sätt som Ryssland, Kina etc.)
USA kunde stänga av moln-leveranser till tex Venezuela. Det skulle kunna hända Sverige, vad gör man då? Staten måste hantera även låga risker med stora konsekvenser)

Detta leder till kraven för att kunna utnyttja molntjänst:

* Uppfylla tekniska och funktionella krav
* Laglig
* Lämplig
* Långsiktigt hållbar.

Vi ska inte ta risken som leverantören ska ta, vi betalar för tjänsten.

## Christian Landgren, ITeams - How to start a movement

Historiken om Stockholms Skolplattform och vidare mot Öppenskollplattform. (Jag hoppade av för annat möte.)
